<?php

namespace Drupal\date_recur\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for date recur occurrence handler plugin manager.
 */
interface DateRecurOccurrenceHandlerManagerInterface extends PluginManagerInterface {

}
